import clearbit
from django.conf import settings


def user_additional_data(data):
    # in this method we can get additional info such as avatar, location, bio, link, urls, etc.
    clearbit.key = settings.CLEARBIT_KEY
    response = clearbit.Person.find(email='alex@clearbit.com', stream=True)
    data['avatar'] = response['avatar']
    return data

